unit UntFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Buttons, UntExcecao;

type
  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TMemo;
    procedure BitBtn1Click(Sender: TObject);
  private
  public
    procedure AfterConstruction; override;
    destructor Destroy; override;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.AfterConstruction;
begin
  inherited;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  Label1.Lines.Clear;
  Label1.Lines.Add(IntToStr(StrToInt('a')));
end;

destructor TForm1.Destroy;
begin
  inherited;
end;

end.